import 'package:flutter/material.dart';
import 'package:toko_komputer/object/komputer_item_object.dart';
import 'package:toko_komputer/screen/about.dart';
import 'package:toko_komputer/screen/entryform.dart';
// import 'car_detail_screen.dart';
// import 'order_screen.dart';
import 'login_screen.dart';
import 'package:flutter/cupertino.dart';
// import '../constants/page_routs.dart';
// import 'calendar_screen.dart';

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage>
    with SingleTickerProviderStateMixin {
  int selectedPage = 0;
  TabController _tabController;
  PageController _pageController;

  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  bool isSearching = false;

  List<KomputerItemObject> stocks = [

    KomputerItemObject(
        imageName: "assets/images/laptop.png",
        stockName: "ROG Zephyrus M15",
        priceName: "Rp. 28.999.999",
        description:
            "Kreator, streamer dan gamer dapat memanfaatkan keahlian mereka ke level yang lebih tinggi dengan ROG Zephyrus M15 yang ramping dan pengalaman Windows 10 yang serbaguna. CPU Intel Core i7 Generasi 10th dan GPU NVIDIA® GeForce® GTX 1660 terbaru digunakan untuk tugas intensif seperti, render 3D, edit video, dan game AAA, sementara penyimpanan SSD 1TB RAID 0 mempercepat waktu load semua aplikasi koleksi Anda. Pilih antara layar beresolusi 4K sangat tinggi yang mencakup 100% ruang warna Adobe RGB atau panel 240Hz/3ms yang membuat aksi gerakan cepat tampak lembut seperti sutera. Audio Hi-Res ESS Sabre memastikan kekayaan, kesetiaan, lagu dan stream untuk suara game Anda.",
        backgroudColor: Colors.black),
    KomputerItemObject(
        imageName: "assets/images/monitor.png",
        stockName: "Rog Swift PG348Q",
        priceName: "Rp. 10.000.000",
        description:
            "ASUS ROG Swift Curved PG348Q Gaming Monitor - 34 21:9 Ultra-wide QHD (3440x1440), overclockable 100Hz , G SYNC",
        backgroudColor: Colors.red),
    KomputerItemObject(
        imageName: "assets/images/pc.png",
        stockName: "ROG HURACAN G21CX",
        priceName: "Rp. 20.000.000",
        description:
            "ROG HURACAN G21CX - i7-9700k-CPU GeForce-RTX 2070 8GB-CPU 16GB DDR4 2666Mhz-RAM 512 GB+1TB-SSD,HDD WINDOWS 10 HOME-OS.",
        backgroudColor: Colors.purple)
  ];

  @override
  void initState() {
    super.initState();
    _tabController = TabController(initialIndex: 0, length: 7, vsync: this);
    _pageController = PageController(initialPage: 0, viewportFraction: 0.8);
  }

  _makeOrder() {
    // Navigator.push(context, MaterialPageRoute(builder: (_) => OrderScreen()));
  }

  _logOut() {
    Navigator.pushReplacement(
        context, MaterialPageRoute(builder: (_) => LoginScreen()));
  }

  _searchPressed() {
    isSearching = !isSearching;
    setState(() {});
  }

  Widget _stockSelector(int index) {
    return AnimatedBuilder(
      animation: _pageController,
      builder: (BuildContext context, Widget widget) {
        double value = 1;
        if (_pageController.position.haveDimensions) {
          value = _pageController.page - index;
          value = (1 - (value.abs() * 0.3)).clamp(0.0, 1.0);
        }
        return Center(
          child: SizedBox(
            height: Curves.easeInOut.transform(value) * 500,
            width: Curves.easeInOut.transform(value) * 400,
            child: widget,
          ),
        );
      },
      child: GestureDetector(
        onTap: () {
          // Navigator.push(context,
          //     FadeRoute(page: CarDetailScreen(car: stock[selectedPage])));
        },
        child: Stack(
          children: <Widget>[
            Container(
              height: 400,
              margin: EdgeInsets.only(left: 20, right: 20),
              decoration: BoxDecoration(
                  gradient: LinearGradient(
                      colors: [Colors.blue, Colors.purpleAccent], stops: [0, 1]),
                  borderRadius: BorderRadius.circular(25)),
              child: Stack(
                children: <Widget>[
                  Center(
                    child: Hero(
                      tag: stocks[index].imageName,
                      child: Image(
                        fit: BoxFit.cover,
                        image: AssetImage(stocks[index].imageName),
                      ),
                    ),
                  ),

                  Positioned(
                    bottom: 25,
                    left: 20,
                    child: Column(
                      children: <Widget>[
                        Text(stocks[index].stockName, 
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 25,
                                fontWeight: FontWeight.bold)),
                  Positioned(
                    bottom: 25,
                    left: 20,
                    child: Column(
                      children:<Widget>[
                        Text(stocks[index].priceName,
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 25,
                            fontWeight: FontWeight.bold)),
                          ],
                         )
                       ,)
                      ],
                    ),
                  ),
                ],
              ),
            ),
            
            Positioned(
              bottom: 10,
              left: 10,
              right: 10,
              child: RawMaterialButton(
                padding: EdgeInsets.all(15),
                child: Icon(
                  Icons.add_shopping_cart,
                  size: 30,
                  color: Colors.white,
                ),
                fillColor: Colors.black,
                shape: CircleBorder(),
                elevation: 2.0,
                onPressed: () => _makeOrder(),
              ),
            )
          ],
        ),
      ),
    );
  }

  _mainWidgetSwitcher(bool isSearching) {
    return !isSearching
        ? Column(
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Hero(
                    tag: "drawer_button",
                    child: Padding(
                      padding: const EdgeInsets.only(left: 25.0),
                      child: IconButton(
                          icon: Icon(Icons.menu, size: 30),
                          onPressed: () =>
                              _scaffoldKey.currentState.openDrawer()),
                    ),
                  ),
                  Padding(
                      padding: const EdgeInsets.only(right: 25.0),
                      child: IconButton(
                          icon: Icon(
                            Icons.search,
                            size: 30,
                          ),
                          onPressed: () => _searchPressed()))
                ],
              ),
              Container(
                padding: const EdgeInsets.all(25.0),
                child: Text(
                  "Best ASUS ROG",
                  style: TextStyle(fontWeight: FontWeight.w600, fontSize: 20),
                ),
              ),
              TabBar(
                controller: _tabController,
                indicatorColor: Colors.transparent,
                labelColor: Colors.black,
                unselectedLabelColor: Colors.grey.withOpacity(0.6),
                labelPadding: EdgeInsets.symmetric(horizontal: 35),
                isScrollable: true,
                tabs: <Widget>[
                  Tab(
                    child: Text(
                      "Laptop",
                      style:
                          TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                    ),
                  ),
                  Tab(
                    child: Text(
                      "Monitor",
                      style:
                          TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                    ),
                  ),
                  Tab(
                    child: Text(
                      "PC",
                      style:
                          TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                    ),
                  ),
                  Tab(
                    child: Text(
                      "VGA",
                      style:
                          TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                    ),
                  ),
                  Tab(
                    child: Text(
                      "MOUSE",
                      style:
                          TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                    ),
                  ),
                  Tab(
                    child: Text(
                      "KEYBOARD",
                      style:
                          TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                    ),
                  ),
                  Tab(
                    child: Text(
                      "ACCECORIS",
                      style:
                          TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 20,
              ),
              Container(
                height: 450,
                width: double.infinity,
                child: PageView.builder(
                  controller: _pageController,
                  onPageChanged: (int index) {
                    setState(() {
                      this.selectedPage = index;
                    });
                  },
                  itemCount: stocks.length,
                  itemBuilder: (BuildContext context, int index) {
                    return _stockSelector(index);
                  },
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text("Description:",
                    style: TextStyle(fontWeight: FontWeight.bold)),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(stocks[selectedPage].description),
              )
            ],
          )
        : Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Row(
                children: <Widget>[
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.only(left: 20.0),
                      child: TextField(
                        onSubmitted: (String text) {
                          if (text.length == 0) {
                            _searchPressed();
                          }
                        },
                        autofocus: true,
                        decoration: InputDecoration(
                            border: InputBorder.none,
                            hintText: 'Look for ASUS ROG'),
                      ),
                    ),
                  ),
                  Padding(
                      padding: const EdgeInsets.only(right: 25.0),
                      child: IconButton(
                          icon: Icon(
                            Icons.search,
                            size: 30,
                          ),
                          onPressed: () => _searchPressed())),
                ],
              ),
              InkWell(
                onTap: () {
                  // Navigator.push(
                  //     context,
                  //     MaterialPageRoute(
                  //         builder: (_) =>
                  //             KomputerDetailScreen(Komputer: stocks[selectedPage])));
                },
                child: Padding(
                  padding: const EdgeInsets.only(left: 15.0, top: 20),
                  child: Container(
                      width: double.infinity, child: Text("Sugestion 1")),
                ),
              ),
              InkWell(
                child: Padding(
                  padding: const EdgeInsets.only(left: 15.0, top: 20),
                  child: Container(
                      width: double.infinity, child: Text("Sugestion 2")),
                ),
              ),
              InkWell(
                onTap: () {
                  // Navigator.push(
                  //     context,
                  //     MaterialPageRoute(
                  //         builder: (_) =>
                  //             KomputerDetailScreen(car: stock[selectedPage])));
                },
                child: Padding(
                  padding: const EdgeInsets.only(left: 15.0, top: 20),
                  child: Container(
                      width: double.infinity, child: Text("Sugestion 3")),
                ),
              ),
            ],
          );
  }

  Widget stockSelector(int index) => _stockSelector(index);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldKey,
        body: Container(
          width: double.infinity,
          child: SafeArea(
            child: SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  AnimatedSwitcher(
                      duration: Duration(milliseconds: 300),
                      child: _mainWidgetSwitcher(isSearching))
                ],
              ),
            ),
          ),
        ), // This
        drawer: Drawer(
          elevation: 10,
          child: Container(
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    colors: [Colors.blue, Colors.purple],
                    stops: [0, 1],
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter)),
            child: SafeArea(
                child: Stack(
              children: <Widget>[
                Hero(
                  tag: "drawer_button",
                  child: Padding(
                    padding: const EdgeInsets.only(left: 25.0),
                    child: IconButton(
                        icon: Icon(Icons.arrow_back, size: 30),
                        onPressed: () => Navigator.of(context).pop()),
                  ),
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Center(
                        child: Container(
                      width: double.infinity,
                      height: 150,
                      child: Stack(
                        children: <Widget>[
                          // Positioned(
                          //   bottom: 50,
                          //   child: Container(
                          //     color: Colors.black,
                          //     width: 400,
                          //     height: 150,
                          //   ),
                          // ),
                          Center(
                            child: Container(
                              height: 150,
                              width: 150,
                              decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  image: new DecorationImage(
                                    fit: BoxFit.cover,
                                    image:AssetImage(
                                      'assets/images/logo1.png',
                                    ),
                                  )),
                            ),
                          ),
                        ],
                      ),
                    )),
                    // Center(
                    //   child: Column(
                    //       crossAxisAlignment: CrossAxisAlignment.start,
                    //       children: <Widget>[
                    //         Text("Anggie Dwi Permana"),
                    //         Text("18282002")
                    //       ]),
                    // ),
                    Padding(
                      padding: EdgeInsets.all(10),
                      child: InkWell(
                        onTap: () {
                          _switchToCalendar();
                        },
                        child: Container(
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius:
                                  BorderRadius.all(Radius.circular(25))),
                          child: Padding(
                            padding: const EdgeInsets.all(15.0),
                            child: Center(
                                child: Text("About",
                                    style: TextStyle(color: Colors.grey))),
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.all(10),
                      child: InkWell(
                        onTap: (){
                          Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (_) =>
                      Entryform()));
                        },
                        child: Container(
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius:
                                  BorderRadius.all(Radius.circular(25))),
                          child: Padding(
                            padding: const EdgeInsets.all(15.0),
                            child: Center(
                                child: Text("Input Data",
                                    style: TextStyle(color: Colors.grey))),
                          ),
                        ),
                      ),
                    ),
                    InkWell(
                      onTap: () => _logOut(),
                      child: Container(
                        decoration: BoxDecoration(color: Colors.transparent),
                        child: Padding(
                          padding: const EdgeInsets.all(15.0),
                          child: Center(
                              child: Text("View profile",
                                  style: TextStyle(color: Colors.white))),
                        ),
                      ),
                    ),
                    InkWell(
                      onTap: () => _logOut(),
                      child: Container(
                        decoration: BoxDecoration(color: Colors.transparent),
                        child: Padding(
                          padding: const EdgeInsets.all(15.0),
                          child: Center(
                              child: Text("Logout",
                                  style: TextStyle(color: Colors.white))),
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            )),
          ),
        ) // trailing comma makes auto-formatting nicer for build methods.
        );
  }
  _switchToCalendar(){
    // Navigator.of(context).push(
    //   FadeRoute(
    //     page: CalendarScreen(),
    //   ),
    // );
     Navigator.push(context, MaterialPageRoute(builder: (_)=>Aboutpage()));
  }
}
