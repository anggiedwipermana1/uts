import 'package:flutter/material.dart';

class KomputerItemObject {
  String imageName;
  String stockName;
  String priceName;

  String description;
  Color backgroudColor;

  KomputerItemObject( {
    this.imageName,
    this.stockName,
    this.priceName,
    this.description = "",
    this.backgroudColor = Colors.green,
  });
}